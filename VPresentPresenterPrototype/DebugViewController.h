//
//  DebugViewController.h
//  VPServerPrototype
//
//  Created by LEUNG Chak Hang on 24/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebugViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *debugText;
@property (weak, nonatomic) NSString* debugString;

@end
