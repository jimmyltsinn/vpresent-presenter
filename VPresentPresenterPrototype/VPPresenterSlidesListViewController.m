//
//  VPPresenterSlidesListViewController.m
//  VPresentPresenterPrototype
//
//  Created by Jimmy Sinn on 29/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPPresenterSlidesListViewController.h"

@interface VPPresenterSlidesListViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;

@property (nonatomic) NSArray* sectionTitle;
@property (nonatomic) NSString* tempFilename;

@property (nonatomic) VPPresenterSplitViewController* splitVC;
@property (nonatomic) VPPresenterDetailViewController* detailVC;

@end

/* Entity
   - Select index file
   - Enter another list ? for all slides preview
   - Highlight current slide
   - Now not able to jump to another slide ... Or may do this but need more consideration
 */

@implementation VPPresenterSlidesListViewController

@synthesize sectionTitle;
@synthesize tempFilename;

@synthesize splitVC;
@synthesize detailVC;

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.splitVC = (VPPresenterSplitViewController*)self.splitViewController;
    self.detailVC =[self.splitVC.viewControllers objectAtIndex:1];
    [self loadContent];
}

- (void)loadContent {
    self.sectionTitle = [[NSArray alloc]initWithObjects:@"PDF File List", nil];
    self.tempFilename = nil;
}

- (void)reloadFileListView {
    NSIndexSet* tempSection = [[NSIndexSet alloc]initWithIndex:1];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationNone];
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.title = @"Slides";
    [self.splitVC listPDFFilesFromDocumentsFolder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return self.splitVC.pdfList.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch( [indexPath section] ) {
        // Section 0: Index File Name and load button
        case 0: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Load Index File Cell"];
            if (aCell == nil) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Load Index File Cell"];
            }
            aCell.textLabel.text = [[[self.splitVC.pdfList objectAtIndex:[indexPath row]]lastPathComponent]stringByDeletingPathExtension];
            aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            return aCell;
        } break;
    }
    
    return nil;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.sectionTitle objectAtIndex:section];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}
*/


/* Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    //TODO Make reload image process back to detail VC
    switch ([indexPath section]) {
        case 0:{
            NSURL* pdfURL = [NSURL fileURLWithPath: [self.splitVC.pdfList objectAtIndex:[indexPath row]]];
            PSPDFDocument *pdfDocument = [PSPDFDocument PDFDocumentWithURL:pdfURL];
            [self.detailVC loadPDF:pdfDocument];
            [self.splitVC.networkAgent sendPDFData:pdfDocument];
        } break;
    }
    //NSIndexSet* tempSection = [[NSIndexSet alloc]initWithIndex:1];
    //[tableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma UITextField delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.tempFilename = textField.text;
}

- (void)viewDidUnload {
    [self setMainTableView:nil];
    [super viewDidUnload];
}
@end
