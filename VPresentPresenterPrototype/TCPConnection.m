//
//  Client.m
//  ConnectionClientDemo
//
//  Created by Jimmy Sinn on 22/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//
//#import <linux/tcp.h>
#import <netinet/tcp.h>

#import "TCPConnection.h"

#import "NetworkUtilities.h"

@interface TCPConnection()
@property (nonatomic) NSFileHandle *fileHandle;
@property (nonatomic) CFSocketRef socket;
@property (nonatomic) id receiveCallbackObject;
@property (nonatomic) SEL receiveCallbackSelector;
@property (nonatomic) id connectionClosedByExceptionCallbackObject;
@property (nonatomic) SEL connectionClosedByExceptionCallbackSelector;
@property (nonatomic) id loggingObject;
@property (nonatomic) SEL loggingSelector;
@property (nonatomic) struct sockaddr_in server;
@property (nonatomic) int state;
@end


@implementation TCPConnection

@synthesize socket;
@synthesize state;
@synthesize foreignHost;
@synthesize fileHandle;
@synthesize loggingObject;
@synthesize loggingSelector;
@synthesize connectionClosedByExceptionCallbackObject;
@synthesize connectionClosedByExceptionCallbackSelector;
@synthesize receiveCallbackObject;
@synthesize receiveCallbackSelector;
@synthesize server;

int port = 43450;

/* Debug purpose */
- (void) registerLog: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
        NSLog(@"Invalud selector for log. Reset");
        self.loggingObject = nil;
        self.loggingSelector = nil;
    } else {
        self.loggingObject = object;
        self.loggingSelector = selector;
    }
    return;
}

- (void) logMessage: (NSString *) msg {
    [self.loggingObject performSelector: loggingSelector withObject: msg];
    return;
}

- (NSString *) foreignHost {
    return [NetworkUtilities getAddressString:self.server];
}

/* Connect method */
- (void) setPort: (unsigned short) port {
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_len = sizeof(addr);
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);
    
    bind(CFSocketGetNative(self.socket), (struct sockaddr *) &addr, sizeof(addr));
    
    return;
}

- (BOOL) connectTo: (struct sockaddr_in) addr {
    CFSocketError err;
    long val = 1;
    
    self.socket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, 0, NULL, NULL);
    
    if (!self.socket) {
        [self logMessage: [NSString stringWithFormat:@"Socket cannot create (%d): %s", errno, strerror(errno)]];
        return NO;
    }
    
    err = setsockopt(CFSocketGetNative(self.socket), SOL_SOCKET, SO_REUSEADDR, &val, sizeof(long));

    if (err) {
        [self logMessage: [NSString stringWithFormat:@"Socket cannot reuse (%d): %s", errno, strerror(errno)]];
    }
    
//    bind(CFSocketGetNative(self.socket), &localAddr, sizeof(localAddr));
    
    self.server = addr;
    
    CFDataRef addrcfd = CFDataCreate(kCFAllocatorDefault, (UInt8 *) &addr, sizeof(addr));
    
    err = CFSocketConnectToAddress(self.socket, addrcfd, 1);
    if (err != kCFSocketSuccess) {
        [self logMessage: [NSString stringWithFormat: @"Cannot connect to server (%ld) %s", err, strerror(errno)]];
        CFRelease(self.socket);
        self.socket = nil;
        return NO;
    }
    
    self.fileHandle = [[NSFileHandle alloc] initWithFileDescriptor:CFSocketGetNative(self.socket) closeOnDealloc:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(receiveData:) name:NSFileHandleDataAvailableNotification object:nil];
    
    [self.fileHandle waitForDataInBackgroundAndNotify];
    
    self.state = 2;
    
    return YES;
}

- (BOOL) connectToIP: (NSString *) ip port: (unsigned short) port hostByteOrder: (BOOL) hostByteOrder {
    struct sockaddr_in addr = [NetworkUtilities getStructSockaddr:ip withPort:port inHostByteOrdering:hostByteOrder];
    return [self connectTo: addr];
}

- (BOOL) connectToFullIP: (NSString *) ipWithPort {
    struct sockaddr_in addr = [NetworkUtilities getStructSockaddr:ipWithPort];
    return [self connectTo: addr];
}

/* Close connection */
- (void) registerConnectionClosedByExceptionCallback: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
        [NSException raise: @"Invalid selector for connection closed by exception" format: @"Object of %@ do not respond to selector %@", [[object class] description], NSStringFromSelector(selector)];
        self.connectionClosedByExceptionCallbackObject = nil;
        self.connectionClosedByExceptionCallbackSelector = nil;
    } else {
        self.connectionClosedByExceptionCallbackObject = object;
        self.connectionClosedByExceptionCallbackSelector = selector;
    }
    return;
}

- (void) closeByException: (NSString *) message {
    [self.connectionClosedByExceptionCallbackObject performSelector: self.connectionClosedByExceptionCallbackSelector withObject: message];
    [self close];
    return;
}

- (void) close {
    CFSocketInvalidate(self.socket);
    CFRelease(self.socket);
    [[NSNotificationCenter defaultCenter] removeObserver: self name: NSFileHandleDataAvailableNotification object: nil];
    [self.fileHandle closeFile];
    self.fileHandle = nil;
    
    memset(&server, 0, sizeof(server));
    
    [self logMessage: @"Connection closed"];
    
    return;
}

/* Handle data receive */
- (void) registerReceiveCallback: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
        [NSException raise: @"Invalid selector for receive data" format: @"Object of %@ do not respond to selector %@", [[object class] description], NSStringFromSelector(selector)];
        self.receiveCallbackObject = nil;
        self.receiveCallbackSelector = nil;
    } else {
        self.receiveCallbackObject = object;
        self.receiveCallbackSelector = selector;
    }
    return;
}

- (void) receiveData: (NSNotification *) notification {
    NSFileHandle *receivingFileHandle = [notification object];
    NSData *data;
    
    @try {
        data = [receivingFileHandle availableData];
    } @catch (NSException *e) {
        [self closeByException: [e reason]];
        return;
    }
    
    if ([data length] == 0) {
        /* Handle server disconnect */
        [self closeByException: @"Connection closed by server"];
        return;
    }
    
    [self logMessage: [NSString stringWithFormat: @"Receive Data (len = %d) > ", [data length]]];
    [self.receiveCallbackObject performSelector: self.receiveCallbackSelector withObject: data];
    [self.fileHandle waitForDataInBackgroundAndNotify];
    
    return;
}

/* Send data */
- (void) sendData: (NSData *) data {
    [self.fileHandle writeData: data];
    return;
}

/* Ininitalization */
- (TCPConnection *) init {
    self = [super init];
    self.state = 1;
    return self;
}

@end
