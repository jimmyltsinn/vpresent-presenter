//
//  VPPresenterServerViewController.m
//  VPresentPresenterPrototype
//
//  Created by Jimmy Sinn on 29/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPPresenterServerViewController.h"

@interface VPPresenterServerViewController ()

@property (nonatomic, strong) NSArray* sectionTitle;
@property (nonatomic, strong) VPPresenterSplitViewController* splitVC;

@property (nonatomic, strong) NSString* tempAddress;

@end

/* Entity
   - Server IP
   - Server Status: Unreachable / Reachable / Connected
*/

@implementation VPPresenterServerViewController

@synthesize sectionTitle;
@synthesize splitVC;
@synthesize tempAddress;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.splitVC = (VPPresenterSplitViewController*)self.splitViewController;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    self.mainTableView = self.tableView;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self loadContent];

}

- (void)loadContent
{
    if(self.sectionTitle == nil){
        self.sectionTitle = [[NSArray alloc]initWithObjects:@"Server Address", @"Connection Status", @"Requests", nil];
    }
    self.tempAddress = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.title = @"Server";
    return; 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 2;
            break;
        
        case 1:
            return 1;
            break;
        
        case 2:
            return 2;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch( [indexPath section] ) {
        // Section 0: IP Address input and connect
        case 0: {
            switch ([indexPath row]) {
                case 0:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"IP Input Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IP Input Cell"];
                    }
                    aCell.textLabel.text = @"IP Address:";
                    aCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    UITextField *addressTextField = (UITextField*)[aCell.contentView viewWithTag:202];
                    if(addressTextField != nil){
                        [[aCell.contentView viewWithTag:202]removeFromSuperview];
                    }
                    
                    addressTextField = [[UITextField alloc] initWithFrame:CGRectMake(120, 12, 185, 30)];
                    addressTextField.tag = 202; // arbitray value only
                    addressTextField.delegate = self;
                    addressTextField.adjustsFontSizeToFitWidth = YES;
                    addressTextField.textColor = [UIColor blackColor];

                    if (!self.tempAddress)
                        self.tempAddress = [self.splitVC serverAddress];
                    addressTextField.text = self.tempAddress;
                    
                    addressTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                    addressTextField.returnKeyType = UIReturnKeyDefault;
                    
                    addressTextField.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    addressTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
                    addressTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
                    addressTextField.textAlignment = UITextAlignmentLeft;
                    
                    addressTextField.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
                    [addressTextField setEnabled: YES];
                    
                    [aCell addSubview:addressTextField];
                    return aCell;
                }break;
                    
                case 1:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Connect Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Connect Cell"];
                    }
                    aCell.textLabel.text = @"Connect and Register";
                    aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                    return aCell;
                }break;
            }
        }break;
        
        // Section 1: Connection Status
        case 1:{
            switch ([indexPath row]) {
                case 0:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Connection Status Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Connection Status Cell"];
                    }
                    //aCell.textLabel.text = @"(server ip)";
                    if(self.splitVC.status == Disconnected)
                        aCell.textLabel.text = @"Disconnected";
                    else
                    if(self.splitVC.status == Connected)
                        aCell.textLabel.text = @"Connected";
                    else
                    if(self.splitVC.status == Presenting)
                        aCell.textLabel.text = @"Presenting";
                    
                    aCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    aCell.userInteractionEnabled = NO;
                    
                    return aCell;
                }break;
            }
        }break;
            
        case 2:{
            switch ([indexPath row]) {
                case 0:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Request Control Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Requeset Control Cell"];
                    }
                    aCell.textLabel.text = @"Request Control";
                    aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                    return aCell;
                }break;
                    
                case 1:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Request Control Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Requeset Control Cell"];
                    }
                    aCell.textLabel.text = @"Unregister and Disconnect";
                    aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                    return aCell;
                }break;
            }
        }break;
    }
    
    return nil;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionTitle objectAtIndex:section];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    switch ([indexPath section]) {
        case 0:{
            switch ([indexPath row]) {
                case 1:{
                    // Lost editing focus
                    [self.view endEditing:YES];
                    
                    if(self.splitVC.status == Disconnected || 1){
                        // Get server address
                        self.splitVC.serverAddress = tempAddress;
                        
                        // Connect to server
                        [self.splitVC startConnection];
                    } else {
                        [self showErrorPrompt:@"Error" description:@"Aleady connected to the moderator"];
                    }
                }
            }
        }break;
            
        case 2: {
            switch ([indexPath row]) {
                // Request control
                case 0:{
                    [self.splitVC.networkAgent sendControlRequest];
                }break;
                
                // Unregister and disconnect
                case 1:{
                    if(self.splitVC.status == Presenting){
                        //NSLog(@"Cannot disconnect when presenting");
                        [self showErrorPrompt:@"Error" description:@"Cannot disconnect when presenting"];
                    }else
                        [self.splitVC closeConnection];
                }break;
            }
        }break;
    }
    //NSArray* tempArray = [[NSArray alloc]initWithObjects:[NSIndexPath indexPathForRow:0 inSection:1], nil];
    //[tableView reloadRowsAtIndexPaths:tempArray withRowAnimation:YES];
    //NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:1];
    //[tableView reloadSections:tempSection withRowAnimation:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma UITextField delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
   self.tempAddress = textField.text;
}

- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
