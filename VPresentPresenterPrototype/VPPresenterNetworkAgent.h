//
//  VPPresenterNetworkAgent.h
//  VPPresenterPrototype
//
//  Created by Jimmy Sinn on 25/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCPConnection.h"
#import <PSPDFKit/PSPDFKit.h>

@protocol VPPresenterNetworkAgentDelegate <NSObject>

- (void)registerCompleted;
- (void)registerFailed;
- (void)unregisterCompleted; 
- (void)requestControlCompleted;
- (void)controlGranted;
- (void)controlWithdraw; 
- (void)slideSendCompleted: (short)slideNumber;
- (void)controlSignalCompleted;
- (void)controlSignalFailed: (char)errorCode detail: (NSData *)detail;
- (BOOL)drawPathSetting: (UIColor *)color size:(char)size;
- (BOOL)drawPath: (CGPoint)point status: (char)status;

- (void)videoPlayCompleted;
- (void)videoPauseCompleted;
- (void)videoStopCompleted;
- (void)videoGotoCompleted;
- (void)videoSendCompleted;

@end

@interface VPPresenterNetworkAgent : NSObject

@property (nonatomic) id<VPPresenterNetworkAgentDelegate> delegate;

- (VPPresenterNetworkAgent *)initWithConnect: (NSString *)ipWithPort;
- (void)sendRegisterRequest: (NSString* )clientName;
- (void)sendUnregisterRequest;
- (void)sendSlide: (NSData *)imageData size: (CGSize)size;
- (void)sendSlide: (UIImage *)image;
- (void)sendControlRequest;
- (void)sendControlSignalRequest: (char)control withParameter: (const char *) parameters;
- (void)sendControlSignalRequest: (char)control;

- (void)sendControlNextSlide;
- (void)sendControlPreviousSlide;
- (void)sendControlToSlide: (int)imageIndex;
//- (void)sendControlBlackScreen: (BOOL)white;
- (void)sendControlClearDrawing;
//- (void)sendControlPassSecondaryControl: (char)presenterID;
//- (void)sendControlWithdrawSecondaryControl: (int)countdown;
- (void)sendControlReturnControl;

- (void)sendPDFData: (PSPDFDocument *)pdfDocument;
- (void)sendPDFAnnotation: (PSPDFAnnotation *)annotation onPage:(int)page;
- (void)sendPDFViewState: (PSPDFViewState *)viewState; 


//- (void)sendDrawPath: (CGPoint)point start:(BOOL)start end:(BOOL)end;
//- (void)sendDrawPath: (CGPoint)point color:(UIColor *)color size:(char)size start:(BOOL)start end:(BOOL)end;
//- (void)sendDrawPathPoint: (CGPoint)point response: (BOOL)response new: (BOOL)new end: (BOOL)end;
//- (void)sendDrawPathPoint: (CGPoint)point response: (BOOL)response new: (BOOL)new end: (BOOL)end color: (CGColorRef)color size: (char)size;

- (void)sendVideoPlay: (NSString *)videoFilename;
- (void)sendVideoPause;
- (void)sendVideoStop;
- (void)sendVideoGoto: (double) second;
- (void)sendVideo: (NSURL *)videoURL;

- (void)closeConnect;

@end
