//
//  GestureView.h
//  GestureDemo
//
//  Created by Jimmy Sinn on 24/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPCanvas : UIImageView

@property (nonatomic) float thickness;
@property (nonatomic) UIColor *color;
@property (nonatomic, readonly) BOOL update;

- (VPCanvas *) initWithFrame:(CGRect) frame;

- (void) penMove: (CGPoint) currentPoint;
- (void) penUp: (CGPoint) point;
- (void) penDown: (CGPoint) point;
- (void) undoImage;
- (void) redoImage;
@end
