//
//  VPPresenterBasicSettingViewController.m
//  VPresentPresenterPrototype
//
//  Created by Jimmy Sinn on 29/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPPresenterBasicSettingViewController.h"

@interface VPPresenterBasicSettingViewController ()

@property (nonatomic) NSArray* sectionTitle;
@property (nonatomic) NSString* tempUsername;
@property (nonatomic, strong) VPPresenterSplitViewController* splitVC;

@end

/* Entity
   - Presenter Name
   - 

*/

@implementation VPPresenterBasicSettingViewController

@synthesize sectionTitle;
@synthesize tempUsername;
@synthesize splitVC;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.splitVC = (VPPresenterSplitViewController*) self.splitViewController;
    
    [self loadContent];
}

- (void)loadContent {
    if (self.sectionTitle == nil) {
        self.sectionTitle = [[NSArray alloc]initWithObjects:@"Identity", nil];
    }
    self.tempUsername = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.title = @"Basic Configuration"; 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        case 0:{
            switch ([indexPath row]) {
                case 0:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Input Name Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Input Name Cell"];
                    }
                    aCell.textLabel.text = @"Your name";
                    aCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    //aCell.userInteractionEnabled = NO;
                    
                    UITextField *nameTextField = (UITextField*)[aCell.contentView viewWithTag:200];
                    if(nameTextField != nil){
                       [[aCell.contentView viewWithTag:200]removeFromSuperview];
                    }
                    nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(130, 12, 185, 30)];
                    nameTextField.tag = 200; // arbitray value only
                    nameTextField.delegate = self;
                    if (!self.tempUsername) {
                        nameTextField.placeholder = [self.splitVC username];
                        nameTextField.text = @"";
                    }
                    nameTextField.text = tempUsername;
                    nameTextField.adjustsFontSizeToFitWidth = YES;
                    nameTextField.textColor = [UIColor blackColor];
                    
                    nameTextField.keyboardType = UIKeyboardTypeDefault;
                    nameTextField.returnKeyType = UIReturnKeyDefault;
                    
                    nameTextField.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    nameTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
                    nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
                    nameTextField.textAlignment = UITextAlignmentLeft;
                    
                    nameTextField.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
                    [nameTextField setEnabled: YES];
                    
                    [aCell.contentView addSubview:nameTextField];
                    return aCell;
                }break;
                    
                case 1:{
                    UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Change Name Cell"];
                    if( aCell == nil ) {
                        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Change Name Cell"];
                    }
                    aCell.textLabel.text = @"Change name";
                    aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                    return aCell;
                }break;
            }
        }break;
    }
    
    return nil;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [sectionTitle objectAtIndex:section];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    switch ([indexPath section]) {
        case 0:{
            switch ([indexPath row]) {
                case 1:{
                    // Lost editing focus
                    [self.view endEditing:YES];
                    if (self.splitVC.status == Disconnected){
                        // Get user name
                        self.splitVC.username = tempUsername;
                        [self showErrorPrompt:@"Message" description:@"Name Changed"];
                    } else {
                        [self showErrorPrompt:@"Error" description:@"Cannot change name after registered"];
                    }
                }
            }
        }break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma UITextField delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.tempUsername = textField.text;
}

@end
