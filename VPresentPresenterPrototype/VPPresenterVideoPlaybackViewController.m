//
//  VPPresenterPenSettingViewController.m
//  VPresentPresenterPrototype
//
//  Created by Jimmy Sinn on 29/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPPresenterVideoPlaybackViewController.h"
#import "VPPresenterDetailViewController.h"
#import "VPCanvas.h"

@interface VPPresenterVideoPlaybackViewController ()

@property (nonatomic, strong) NSArray* sectionTitles;
//@property (nonatomic, strong) NSArray* videoList;
@property (nonatomic, strong) NSArray* videoControl;

@property (nonatomic, strong) VPPresenterSplitViewController* splitVC;
@property (nonatomic, strong) VPPresenterDetailViewController* detailVC;

@property (nonatomic, strong) UITextField *gotoSecondTextField;

@end

/* Entity
   - Color ... Color picker is included but not sure how to use
   - Pen Size ... Int value / Use slide to set?
*/

@implementation VPPresenterVideoPlaybackViewController

@synthesize sectionTitles;
//@synthesize videoList;
@synthesize videoControl;
@synthesize gotoSecondTextField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.splitVC = (VPPresenterSplitViewController* )self.splitViewController;
    self.detailVC = [self.splitViewController.viewControllers objectAtIndex:1];
    
    self.sectionTitles = [[NSArray alloc]initWithObjects:@"Video List", @"Control", nil];
    //self.videoList = [[NSArray alloc]initWithObjects:@"Red", @"Blue", @"Green", nil];
    
    self.videoControl = [[NSArray alloc]initWithObjects:@"Play", @"Pause", @"Stop", @"Goto", nil];
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.title = @"Video Playback";
    [self.splitVC listVideoFilesFromDocumentsFolder];
    return; 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionTitles objectAtIndex:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return self.splitVC.videoList.count;
            break;
        
        case 1:
            return self.videoControl.count;
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //VPPresenterDetailViewController* detailVC = [self.splitViewController.viewControllers objectAtIndex:1];
    //VPCanvas* canvas = ((VPCanvas *)([detailVC.slides viewWithTag:124]));
    switch( [indexPath section] ) {
        // Section 0: Video List
        case 0: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Video File Cell"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellAccessoryNone reuseIdentifier:@"Video File Cell"];
            }
            aCell.textLabel.text = [[[self.splitVC.videoList objectAtIndex:[indexPath row]]lastPathComponent]stringByDeletingPathExtension];
            
            aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            return aCell;
        }break;
            
        // Section 1: Video Control
        case 1:{
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Video Control Cell"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellAccessoryNone reuseIdentifier:@"Video Control Cell"];
            }
            aCell.textLabel.text = [self.videoControl objectAtIndex:[indexPath row]];
            return aCell;
        }break;
    }
    
    return nil;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)customAlert
{
    UIView *myCustomView = [[UIView alloc] initWithFrame:CGRectMake(20, 450, 280, 120)];
    //myCustomView.center = self.splitVC.view.center;
    [myCustomView setBackgroundColor:[UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.5f]];
   // [myCustomView setAlpha:0.5f];
    
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [dismissButton addTarget:self action:@selector(dismissCustomView:) forControlEvents:UIControlEventTouchUpInside];
    [dismissButton setTitle:@"Go" forState:UIControlStateNormal];
    [dismissButton setFrame:CGRectMake(20, 70, 240, 40)];
    [myCustomView addSubview:dismissButton];
    
    self.gotoSecondTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 240, 35)];
    [self.gotoSecondTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.gotoSecondTextField setKeyboardType:UIKeyboardTypeDecimalPad];
    [myCustomView addSubview:self.gotoSecondTextField];
    
    [self.view addSubview:myCustomView];
    
    [UIView animateWithDuration:0.2f animations:^{
        [myCustomView setAlpha:1.0f];
    }];
}

- (void)dismissCustomView:(UIButton *)sender
{
    [self.splitVC.networkAgent sendVideoGoto:[self.gotoSecondTextField.text doubleValue]];
    [UIView animateWithDuration:0.2f animations:^{
        [sender.superview setAlpha:0.0f];
    }completion:^(BOOL done){
        [sender.superview removeFromSuperview];
    }];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section]) {
        case 0:{
            [self.splitVC.networkAgent sendVideo:[NSURL fileURLWithPath:[self.splitVC.videoList objectAtIndex:[indexPath row]]]];
            [self.splitVC showErrorPrompt:@"Message" description:@"Sending video"];
        } break;
            
        case 1:{
            switch ([indexPath row]) {
                case 0:
                    [self.splitVC.networkAgent sendVideoPlay:@"lol"];
                    break;
                case 1:
                    [self.splitVC.networkAgent sendVideoPause];
                    break;
                case 2:
                    [self.splitVC.networkAgent sendVideoStop];
                    break;
                case 3:
                    [self customAlert];
                    break;
            }
        } break;
    }

    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:[indexPath section]];
    [tableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
