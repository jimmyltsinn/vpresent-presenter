//
//  Client.h
//  ConnectionClientDemo
//
//  Created by Jimmy Sinn on 22/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CFNetwork/CFNetwork.h>
#import <arpa/inet.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>

@interface TCPConnection : NSObject

/**
 * State of TCP connection
 *  0   Connection object initialized
 *  1   Ready for connection
 *  2   Connection Established
 *  3   Connection Closed
 */
@property (nonatomic, readonly) int state;
@property (nonatomic, readonly) NSString *foreignHost;

/* Debug logging */
- (void) registerLog: (id) object selector: (SEL) selector;

/* Connect to server */
- (void) setPort: (unsigned short) port; 
- (BOOL) connectTo: (struct sockaddr_in) addr;
- (BOOL) connectToIP: (NSString *) ip port: (unsigned short) port hostByteOrder: (BOOL) hostByteOrder;
- (BOOL) connectToFullIP: (NSString *) ipWithPort;

/* Close connection */
- (void) registerConnectionClosedByExceptionCallback: (id) object selector: (SEL) selector; 
- (void) close;

/* Receive data handling */
- (void) registerReceiveCallback: (id) object selector: (SEL) selector;

/* Send data */
- (void) sendData: (NSData *) data;

@end
