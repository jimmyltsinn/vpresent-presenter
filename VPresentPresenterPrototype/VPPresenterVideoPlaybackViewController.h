//
//  VPPresenterPenSettingViewController.h
//  VPresentPresenterPrototype
//
//  Created by Jimmy Sinn on 29/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPPresenterSplitViewController.h"

@interface VPPresenterVideoPlaybackViewController : UITableViewController

@end
