//
//  VPModeratorSplitViewController.m
//  VPServerPrototype
//
//  Created by Jimmy Sinn on 28/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPPresenterSplitViewController.h"
#import "VPPresenterDetailViewController.h"
#import "VPPresenterMasterTabBarViewController.h"
#import "VPPresenterServerViewController.h"

#import "NetworkUtilities.h"
#import "Reachablity.h"

#import "Timer.h"

@interface VPPresenterSplitViewController ()

@property (strong, nonatomic) SimplePing *pinger;
@property (strong, nonatomic) NSTimer *pingTimer;

@property (strong, nonatomic) Timer *presentTimer;

@property (strong, nonatomic) VPPresenterServerViewController* serverVC;
@property (strong, nonatomic) VPPresenterDetailViewController* detailVC;

@end

@implementation VPPresenterSplitViewController

@synthesize username;
@synthesize indexFileName;
@synthesize imageList;
@synthesize currentImageIndex;
@synthesize serverAddress;
@synthesize networkAgent;
@synthesize pinger;
@synthesize pingTimer;
@synthesize presentTimer;

@synthesize videoList;

@synthesize pdfList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        // Custom initialization
    }
    return self;
}

- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)logMessage:(NSString *)msg {
    NSLog(@"%@", msg);
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.detailVC = [self.viewControllers objectAtIndex:1];
    
    UINavigationController* naviVC = [self.viewControllers objectAtIndex:0];
    VPPresenterMasterTabBarViewController* masterVC = [naviVC.viewControllers objectAtIndex:0];
    self.serverVC = [masterVC.viewControllers objectAtIndex:3];
    
    // Timer for each presenter
    self.presentTimer = [[Timer alloc]init];
    
    // To be controlled and accessed by lower VCs
    self.username = @"John Smith";
    self.indexFileName = @"Master.lst";
    self.imageList = [[NSMutableArray alloc]init];
    self.currentImageIndex = 0;
    self.serverAddress = @"192.168.100.1:44300";
    
//    self.pinger = [SimplePing simplePingWithHostName:self.serverAddress];
    self.pinger.delegate = self;
    [self.pinger start]; 
}

- (void)startConnection {
    self.networkAgent = [[VPPresenterNetworkAgent alloc] initWithConnect:self.serverAddress];
    self.networkAgent.delegate = self;
    [self.networkAgent sendRegisterRequest:self.username];
    return;
}

- (void)closeConnection {
    [self.networkAgent sendUnregisterRequest];
    // following moved to unregister complete
    //[self.networkAgent closeConnect];
    //self.networkAgent = nil;
}

- (void)clearCurrentDrawing {
    [self.networkAgent sendControlClearDrawing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startPing {
    [self sendPing];
    self.pingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendPing) userInfo:nil repeats:YES];
}

- (void)sendPing {
    [self.pinger sendPingWithData:nil];
}

- (void)simplePing:(SimplePing *)pinger didStartWithAddress:(NSData *)address {
    // Called after the SimplePing has successfully started up.  After this callback, you
    // can start sending pings via -sendPingWithData:
    [self startPing];
}

- (void)simplePing:(SimplePing *)pinger didFailWithError:(NSError *)error {
    // If this is called, the SimplePing object has failed.  By the time this callback is
    // called, the object has stopped (that is, you don't need to call -stop yourself).

    // IMPORTANT: On the send side the packet does not include an IP header.
    // On the receive side, it does.  In that case, use +[SimplePing icmpInPacket:]
    // to find the ICMP header within the packet.
    //NSLog(@"Simple ping failed");
    [self.pingTimer invalidate];
}

- (void)simplePing:(SimplePing *)pinger didFailToSendPacket:(NSData *)packet error:(NSError *)error {
    // Called whenever the SimplePing object tries and fails to send a ping packet.
    //NSLog(@"DidFailedSendPacket");
    [self.pingTimer invalidate];
}

- (void)simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet {
    // Called whenever the SimplePing object receives an ICMP packet that looks like
    // a response to one of our pings (that is, has a valid ICMP checksum, has
    // an identifier that matches our identifier, and has a sequence number in
    // the range of sequence numbers that we've sent out).
    //NSLog(@"Success");
    [self.pingTimer invalidate]; 
}

- (void)simplePing:(SimplePing *)pinger didReceiveUnexpectedPacket:(NSData *)packet {
    // Called whenever the SimplePing object receives an ICMP packet that does not
    // look like a response to one of our pings.
}

-(void)listFilesFromDocumentsFolder{
    //---init a file manager---
    NSFileManager * filemgr = [NSFileManager defaultManager];
    
    //---get the path of the Documents folder---
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullIndexPath = [documentsDirectory stringByAppendingPathComponent:indexFileName];
    
    VPPresenterDetailViewController* detailVC = [self.viewControllers objectAtIndex:1];
    
    if ([filemgr fileExistsAtPath: fullIndexPath ] == YES){
        [detailVC appendLog:@"Index File exists\n"];
        NSData * data = [filemgr contentsAtPath: fullIndexPath];
        [detailVC appendLog: [@"Data size = " stringByAppendingFormat:@"%d\n", data.length]];
        NSString* rawStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSArray* fileList = [rawStr componentsSeparatedByString:@"\n"];
        imageList = [[NSMutableArray alloc]initWithCapacity:[fileList count]];
        
        for (NSString *s in fileList){
            if([s rangeOfString:@".png" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location ||
               [s rangeOfString:@".jpg" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location)
            {
                // Load image file name, add into array list
                [imageList addObject:[documentsDirectory stringByAppendingPathComponent:s]];
            }
        }
        
        [detailVC appendLog:@"Index File Read Complete\n"];
    }
    else{
        [detailVC appendLog:@"Index File not found\n"];
    }
    
    //NSArray *fileList = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    //NSMutableString *filesStr = [NSMutableString stringWithString:@"Image in Documents folder: \n"];
    
}

-(void)listVideoFilesFromDocumentsFolder{
    //---init a file manager---
    NSFileManager * filemgr = [NSFileManager defaultManager];
    
    //---get the path of the Documents folder---
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *fileList = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    self.videoList = [[NSMutableArray alloc]init];
    
    for (NSString *s in fileList){
        if([s rangeOfString:@".m4v"].location != NSNotFound)
        {
            // Load video file name, add into array list
            [videoList addObject:[documentsDirectory stringByAppendingPathComponent:s]];
        }
    }
}

-(void)listPDFFilesFromDocumentsFolder{
    //---init a file manager---
    NSFileManager * filemgr = [NSFileManager defaultManager];
    
    //---get the path of the Documents folder---
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *fileList = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    self.pdfList = [[NSMutableArray alloc]init];
    
    for (NSString *s in fileList){
        if([s rangeOfString:@".pdf"].location != NSNotFound)
        {
            // Load video file name, add into array list
            [pdfList addObject:[documentsDirectory stringByAppendingPathComponent:s]];
        }
    }
}

- (void)reloadServerTableView {
    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:1];
    [self.serverVC.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma VPPresenterNetworkAgentDelegate

- (void)registerCompleted {
    [self logMessage:@"Register Completed"];
    self.status = Connected;
    [self reloadServerTableView];
    [self showErrorPrompt:@"Message" description:@"Connected to server"];
}

- (void)registerFailed {
    [self logMessage:@"Register Failed"];
    self.status = Disconnected;
    [self reloadServerTableView];
    [self showErrorPrompt:@"Error" description:@"Connection Failed"];
}

/* Jack Start */
- (void)videoPlayCompleted {
    NSLog(@"Video Play Completed");
}

- (void)videoPauseCompleted {
    NSLog(@"Video Pause Completed");
    
}

- (void)videoStopCompleted {
    NSLog(@"Video Stop Completed");
    
}

- (void)videoGotoCompleted {
    NSLog(@"Video Goto Completed");
}

- (void)videoSendCompleted {
    NSLog(@"Video Send Completed");
    [self showErrorPrompt:@"Message" description:@"Video sent"];
}
/* Jack End */

- (void)unregisterCompleted {
    [self logMessage:@"Unregister Complete"];
    [self.networkAgent closeConnect];
    self.networkAgent = nil;
    self.status = Disconnected;
    [self reloadServerTableView];
    
    // Set label and timer
    self.detailVC.presenterLabel.text = @"";
    self.detailVC.timerLabel.text = @"00:00:00";
    [self.presentTimer end];
    [self.presentTimer resetEachSecondAction];
    
    [self showErrorPrompt:@"Message" description:@"Unregistered and Disconnected from Server"];
}

- (void)requestControlCompleted {
//#warning imcomplete implementation
    [self logMessage:@"Request Control Complete"];
    [self showErrorPrompt:@"Message" description:@"Request sent."];
    
}

- (void)controlGranted {
    [self logMessage:@"Control Granted!"];
    self.status = Presenting;
    [self reloadServerTableView];
    if(self.imageList.count != 0){
        //VPPresenterDetailViewController* detailVC = [self.viewControllers objectAtIndex:1];
        [self.detailVC loadImage:self.currentImageIndex];
    }
    // Set label and timer
    self.detailVC.presenterLabel.text = @"Presenting";
    [self.presentTimer start];
    [self.presentTimer setEachSecondAction:self selector:@selector(updateTimer)];
    
    [self showErrorPrompt:@"Control Granted" description:@"You may start your presentation"];
}

- (void)controlWithdraw {  
    [self logMessage:@"Control Withdraw!"];
    self.status = Connected;
    [self reloadServerTableView];
    // Set label and timer
    self.detailVC.presenterLabel.text = @"";
    [self.presentTimer pause];
    [self.presentTimer resetEachSecondAction];
    
    [self showErrorPrompt:@"Control Withdraw" description:@"Your presentation session has been suspended"];
}

- (void)slideSendCompleted:(short)slideNumber {
//#warning imcomplete implementation
    [self logMessage:[NSString stringWithFormat:@"Slide Send Completed: %d", slideNumber]];
}

- (void)controlSignalCompleted {
//#warning imcomplete implementation
    NSLog(@"controlSignalCompleted.");
    [self logMessage:@"Control Signal Completed"];
}

- (void)controlSignalFailed: (char)errorCode detail: (NSData *)detail{
//#warning imcomplete implementation
    [self logMessage:[NSString stringWithFormat:@"Control Signal Failed: %d, %@", errorCode, [detail description]]];
}

- (BOOL)drawPathSetting:(UIColor *)color size:(char)size{
//#warning imcomplete implementation
    return YES;
}

- (BOOL)drawPath: (CGPoint)point status:(char)status{
    VPPresenterDetailViewController *detailViewController = [self.viewControllers objectAtIndex:1];
    if (status == 1) {
        [((VPCanvas *)([detailViewController.slides viewWithTag:124])) penDown:point];
    } else if (status == 2) {
        [((VPCanvas *)([detailViewController.slides viewWithTag:124])) penUp:point];
    } else {
        [((VPCanvas *)([detailViewController.slides viewWithTag:124])) penMove:point];
    }
    return YES;
}

- (void)updateTimer {
    self.detailVC.timerLabel.text = [self.presentTimer currentTimeInString];
}

//-(void) displayAlert:(NSString *) str {
//    UIAlertView *alert =
//    [[UIAlertView alloc] initWithTitle:@"Alert"
//                               message:str
//                              delegate:self
//                     cancelButtonTitle:@"OK"
//                     otherButtonTitles:nil];
//    [alert show];
//    //[alert release];
//}

@end