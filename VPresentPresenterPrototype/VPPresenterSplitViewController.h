//
//  VPModeratorSplitViewController.h
//  VPServerPrototype
//
//  Created by Jimmy Sinn on 28/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCPConnection.h"
#import "VPPresenterNetworkAgent.h"
#import "SimplePing.h"

typedef enum {
    Disconnected,
    Connected,
    Presenting 
} ConnectionStatus;

@interface VPPresenterSplitViewController : UISplitViewController<VPPresenterNetworkAgentDelegate, SimplePingDelegate>

//@property (nonatomic, strong) TCPConnection *connection;

// User identifier
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) VPPresenterNetworkAgent *networkAgent;
@property (nonatomic) ConnectionStatus status;

// Index filename and Slides filename list
@property (nonatomic, strong) NSString* indexFileName;
@property (nonatomic, strong) NSMutableArray* imageList;
@property (nonatomic) NSInteger currentImageIndex;
-(void)listFilesFromDocumentsFolder;

// Server IP
@property (nonatomic, strong) NSString* serverAddress;
//@property (nonatomic, strong) NSString* serverPort;

- (void)startConnection;
- (void)closeConnection;

- (void) showErrorPrompt: (NSString *) title description: (NSString *) description;
//-(void) displayAlert:(NSString *) str;

@property (nonatomic, strong) NSMutableArray* videoList;
-(void)listVideoFilesFromDocumentsFolder;

@property (nonatomic, strong) NSMutableArray* pdfList;
-(void)listPDFFilesFromDocumentsFolder;

@end
