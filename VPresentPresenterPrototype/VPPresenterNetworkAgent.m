//
//  VPPresenterNetworkAgent.m
//  VPPresenterPrototype
//
//  Created by Jimmy Sinn on 25/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPPresenterNetworkAgent.h"
#import "TCPConnection.h"

#define bit_set(s, pos) s[pos >> 3] |= 1 << (pos & 7)
#define bit_get(s, pos) (s[pos >> 3] & (1 << (pos & 7)))

#define bitc_set(c, pos) c |= 1 << (pos & 7)
#define bitc_reset(c, pos) c &= ~ (1 << (pos & 7))
#define bitc_get(c, pos) (c & (1 << (pos & 7)))

@interface VPPresenterNetworkAgent()

@property (nonatomic) char presenterID;

@property (nonatomic) TCPConnection *connection;

@property (strong, nonatomic) NSURL *prepareVideoURL;

@end

@implementation VPPresenterNetworkAgent
@synthesize presenterID;
@synthesize connection;
@synthesize prepareVideoURL;

- (VPPresenterNetworkAgent *)initWithConnect: (NSString *)ipWithPort {
    self = [super init];
    self.connection = [[TCPConnection alloc] init];
    
    [self.connection registerReceiveCallback:self selector:@selector(dataReceiveHandler:)];
    
    [self.connection connectToFullIP:ipWithPort];
    
    self.presenterID = 0xFF;
    
    return self;
}

- (void)dataReceiveHandler: (NSData *)data {
    const char *msg = [data bytes];
    
    if (msg[0] != 0x02 && msg[1] != presenterID) {
        [NSException raise:@"Wrong client ID!!" format:@""];
        return;
    }
    
    switch (msg[0]) {
        case 0x02:
        case 0x03:
            [self handleRegisterRespond:data];
            break;
        case 0x05:
            [self handleUnregisterRespond];
            break;
        case 0x07:
            [self handleRequestControlRespond];
            break;
        case 0x08:
            [self handleGrantControl];
            break;
        case 0x09:
            [self handleWithdrawControl];
            break; 
        case 0x0B:
            [self handleSlideRespond:data];
            break;
        case 0x0D:
        case 0x0E:
            [self handleControlSignalResponse:data];
            break;
        case 0x10:
            [self handlePathDrawing:data];
            break;
            
        // video playback part (Jack)
        case 0x21:
            [self handleVideoPlay];
            break;
        case 0x23:
            [self handleVideoPause];
            break;
        case 0x25:
            [self handleVideoStop];
            break;
        case 0x27:
            [self handleVideoGoto];
            break;
        case 0x29:
            [self handleVideoPrepareSend:data];
            break;
        case 0x2B:
            [self handleVideoSend];
            break;
            
        default:
            NSLog(@"Unknown command ... Ignore");
            return;
    }
    
    return;
}

- (void)handleRegisterRespond: (NSData *)data {
    const char *msg = [data bytes];
    
    if (msg[0] == 0x02) {
        self.presenterID = msg[1];
        [self.delegate registerCompleted];
    } else if (msg[0] == 0x03) {
        [self.delegate registerFailed];
    } else {
        NSLog(@"Unknown command in handleRegisterRespond:");
    }
}

/* Jack End */
- (void)handleVideoPlay {
    [self.delegate videoPlayCompleted];
}

- (void)handleVideoPause {
    [self.delegate videoPauseCompleted];
}

- (void)handleVideoStop {
    [self.delegate videoStopCompleted];
}

- (void)handleVideoGoto {
    [self.delegate videoGotoCompleted];
}

- (void)videoReceiveHandler: (NSData*) data {
    
}

- (void)handleVideoPrepareSend: (NSData*)data {
    NSLog(@"video prepare send complete....going to send video");
    
    NSData* videoData = [[NSData alloc]initWithContentsOfURL:self.prepareVideoURL];
    //[self sendVideoData:videoData];
    NSThread* sendVideoThread = [[NSThread alloc]initWithTarget:self selector:@selector(sendVideoData:) object:videoData];
    [sendVideoThread start];
}

- (void)handleVideoSend {
    [self.delegate videoSendCompleted];
}
/* Jack End */

- (void)handleUnregisterRespond {
    [self.delegate unregisterCompleted]; 
}

- (void)handleRequestControlRespond {
    [self.delegate requestControlCompleted]; 
}

- (void)handleGrantControl {
    [self.delegate controlGranted]; 
}

- (void)handleWithdrawControl {
    [self.delegate controlWithdraw];
}

- (void)handleSlideRespond: (NSData *)data {
    const char *msg = [data bytes];
    
    unsigned short slideNum;
    
    memcpy(&slideNum, msg + 8, 2);
    
    [self.delegate slideSendCompleted:slideNum];
}

- (void)handleControlSignalResponse: (NSData*) data {
    const char *msg = [data bytes];
    
    if (msg[0] == 0x0E) {
        [self.delegate controlSignalCompleted];
        //TODO Handle successful signal control
    } else if (msg[0] == 0x0F) {
//        char errorCode = msg[4];
//        char *errorDetail = malloc(3);
//        memcpy(errorDetail, msg + 5, 3);

        [self.delegate controlSignalFailed:msg[8] detail:[data subdataWithRange:NSMakeRange(9, 3)]];
    } else {
        NSLog(@"Unknown command in hhandleControlSignalRequest");
    }
    return;
}


- (void)sendRegisterRequest: (NSString* )clientName {
    unsigned short nameLength = [clientName length];
    unsigned int dataLength = 10 + nameLength;
    char *msg = malloc(10 + dataLength);
    const char *name = [clientName cStringUsingEncoding:NSASCIIStringEncoding];
    
    msg[0] = 0x01;
    msg[1] = 0xFF;
    memset(msg + 2, 0, 2);

    memcpy(msg + 4, &dataLength, 4);
    
    memcpy(msg + 8, &nameLength, 2);
    memcpy(msg + 10, name, nameLength);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:YES];

    //NSLog(@"Register Request Length = %d", dataLength);
    assert([data length] == dataLength);
    
    [self.connection sendData:data];
    
    return;
}

/* Jack Start */
- (void)sendVideoPlay: (NSString*)videoFilename{
    
    NSLog(@"send video play");
    char msg[12];
    msg[0] = 0x20;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    unsigned int length = [videoFilename length];
    memcpy(msg + 8, &length, 4);
    
    length += 12;
    
    memcpy(msg + 4, &length, 4);
    
    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:12 freeWhenDone:NO];
    
    [data appendData:[videoFilename dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%d", [data length]);
    NSLog(@"%d", length);
    assert([data length] == length);
    
    [self.connection sendData:data];
    return ;
}

- (void)sendVideoPause {
    NSLog(@"send video pause");
    
    unsigned int dataLength = 8;
    char *msg = malloc(dataLength);
    
    msg[0] = 0x22;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &dataLength, 4);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:YES];
    
    assert([data length] == dataLength);
    
    [self.connection sendData:data];
    return;
}

- (void)sendVideoStop {
    NSLog(@"send video stop");
    
    unsigned int dataLength = 8;
    char *msg = malloc(dataLength);
    
    msg[0] = 0x24;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &dataLength, 4);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:YES];
    
    assert([data length] == dataLength);
    
    [self.connection sendData:data];
    return;
}

- (void)sendVideoGoto: (double) second {
    NSLog(@"send video goto");
    NSLog(@"%f", second);
    
    unsigned int dataLength = 16;
    char *msg = malloc(dataLength);
    
    msg[0] = 0x26;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &dataLength, 4);
    memcpy(msg + 8, &second, 8);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:YES];
    
    assert([data length] == dataLength);
    
    [self.connection sendData:data];
    return;
}

- (void)sendVideoPrepareSend {
    NSLog(@"send video prepare send");
    
    unsigned int dataLength = 8;
    char *msg = malloc(dataLength);
    
    msg[0] = 0x28;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &dataLength, 4);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:YES];
    
    assert([data length] == dataLength);
    
    [self.connection sendData:data];
    return;
}

- (void)sendVideo:(NSURL *)videoURL {
    self.prepareVideoURL = videoURL;
    [self sendVideoPrepareSend];
}


- (void)sendVideoData: (NSData *)videoData {
    NSLog(@"send video data");
    char msg[12];
    msg[0] = 0x2A;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    unsigned int length = [videoData length];
    memcpy(msg + 8, &length, 4);
    
    length += 12;
    
    memcpy(msg + 4, &length, 4);
    
    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:12 freeWhenDone:NO];
    
    [data appendData:videoData];
    
    NSLog(@"%d", [data length]);
    NSLog(@"%d", length);
    assert([data length] == length);
    
    [self.connection sendData:data];
    //    TCPConnection* videoConnection = [[TCPConnection alloc] init];
    //
    //    [videoConnection registerReceiveCallback:self selector:@selector(dataReceiveHandler:)];
    //
    //    [videoConnection connectToFullIP:@"192.168.100.1:44400"];
    //
    //    [videoConnection sendData:data];
    
    return;
}
/* Jack End*/

- (void)sendUnregisterRequest {
    char msg[8];
    unsigned int dataLength = 8;
    
    msg[0] = 0x04;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    memcpy(msg + 4, &dataLength, 4);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:NO];
    [self.connection sendData:data];
    
    assert([data length] == dataLength);
    
    return;
}

- (void)sendSlide: (NSData *)imageData size: (CGSize)size {
    char msg[16];
    msg[0] = 0x0A;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    unsigned short tmp = size.height;
    memcpy(msg + 8, &tmp, 2);
    tmp = size.width;
    memcpy(msg + 10, &tmp, 2);
    
    unsigned int length = [imageData length];
    memcpy(msg + 12, &length, 4);
    
    length += 16;
    
    memcpy(msg + 4, &length, 4);
    
    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:16 freeWhenDone:NO];
    
    [data appendData:imageData];
    
    assert([data length] == length);
    
    [self.connection sendData:data];
    
    return;
}

- (void)sendPDFData: (PSPDFDocument *)pdfDocument {
    NSData *pdfData = [NSData dataWithContentsOfURL:[pdfDocument fileURL]];
    //[[NSFileManager defaultManager] contentsAtPath:[NSString stringWithContentsOfURL:[pdfDocument basePath] encoding:<#(NSStringEncoding)#> error:<#(NSError *__autoreleasing *)#>[pdfDocument basePath] ];
//    NSData *pdfData = [NSKeyedArchiver archivedDataWithRootObject:pdfDocument];
    
//    NSData *pdfData = [pdfDocument data];
//    NSLog(@"pdfData = %@", [pdfData description]);
    char msg[8];
    unsigned int len = 8 + [pdfData length];
    
    msg[0] = 0x30;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &len, 4);
    
    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:8 freeWhenDone:NO];
    [data appendData:pdfData];
    [self.connection sendData:data];
    return; 
}

- (void)sendPDFAnnotation: (PSPDFAnnotation *)annotation onPage:(int)page {
    NSData *annotationData = [NSKeyedArchiver archivedDataWithRootObject:annotation];

    char msg[12];
    unsigned int len = 12 + [annotationData length];
    msg[0] = 0x32;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &len, 4);
    memcpy(msg + 8, &page, 4);

    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:12 freeWhenDone:NO];
    [data appendData:annotationData];
    [self.connection sendData:data];
}

- (void)sendPDFViewState:(PSPDFViewState *)viewState {
    NSData *viewStateData = [NSKeyedArchiver archivedDataWithRootObject:viewState];

    NSLog(@"hi");
    char msg[8];
    unsigned int len = 8 + [viewStateData length];
    msg[0] = 0x34;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &len, 4);
    
    NSMutableData *data = [NSMutableData dataWithBytesNoCopy:msg length:8 freeWhenDone:NO];
    [data appendData:viewStateData];
    
    [self.connection sendData:data]; 
}

- (void)sendSlide: (UIImage *)image {
    [self sendSlide:UIImagePNGRepresentation(image) size:image.size];
}

- (void)sendControlRequest {
    unsigned int dataLength = 8;
    char msg[8];
    msg[0] = 0x06;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    memcpy(msg + 4, &dataLength, 4);
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:NO];
    [self.connection sendData:data];
    assert([data length] == dataLength);

    return;
}

- (void)sendControlSignalRequest: (char)control withParameter: (const char *) parameters {
    unsigned int dataLength = 12;
    char msg[12];
    msg[0] = 0x0C;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    memcpy(msg + 4, &dataLength, 4);
    
    msg[8] = control;
    if (parameters == NULL) {
        memset(msg + 9, 0, 3);
    } else {
        memcpy(msg + 9, parameters, 3);
    }
    
    NSData *data = [[NSData alloc] initWithBytesNoCopy:msg length:dataLength freeWhenDone:NO];
    [self.connection sendData:data];
    assert([data length] == dataLength);
 
    return;
}

- (void)sendControlNextSlide {
    [self sendControlSignalRequest:0x01];
    return;
}

- (void)sendControlPreviousSlide {
    [self sendControlSignalRequest:0x02];
}

- (void)sendControlToSlide: (int)imageIndex {
    char parameter[3];
    short tmp = imageIndex;
    NSLog(@"image Index = %d", tmp);
    memcpy(parameter + 1, &tmp, 2);
    [self sendControlSignalRequest:0x03 withParameter:parameter];
    return;
}

- (void)sendControlBlackScreen: (BOOL)white {
    if (white) {
        char parameter[3];
        //TODO parameter
        [self sendControlSignalRequest:0xF0 withParameter:parameter];
    } else {
        [self sendControlSignalRequest:0xF0];
    }
    return;
}

- (void)sendControlClearDrawing {//: (int)imageIndex {
    char parameter[3];
    [self sendControlSignalRequest:0xF1 withParameter:parameter];
    return;
}

- (void)sendControlPassSecondaryControl: (char)presenterID {
    char parameter[3];
    //TODO parameter
    [self sendControlSignalRequest:0xFD withParameter:parameter];
    return; 
}

- (void)sendControlWithdrawSecondaryControl: (int)countdown {
    char parameter[3];
    //TODO parameter
    [self sendControlSignalRequest:0xFE withParameter:parameter];
    return;
}

- (void)sendControlReturnControl {
    [self sendControlSignalRequest:0xFF];
    return;
}

- (void)sendControlSignalRequest: (char)control {
    [self sendControlSignalRequest:control withParameter:NULL];
    return;
}

- (void)sendDrawPath: (CGPoint)point start:(BOOL)start end:(BOOL)end{
    NSData *data = [self dataDrawingPath:point start:start end:end ack:NO];
    [self.connection sendData:data];
    return;
}

- (void)sendDrawPath: (CGPoint)point color:(UIColor *)color size:(char)size start:(BOOL)start end:(BOOL)end {
    //NSLog(@"hii");
    NSData *data = [self dataDrawingPath:point color:color size:size start:start end:end ack:NO];
    
    //NSLog([self drawingIsSet:((const char*)[data bytes])[8]] ? @"YES" : @"NO");
    
    //NSLog(@"Size: %d - %d", size, [self drawingGetSize:([data bytes] + 12)]);
    
    [self.connection sendData:data];
    return;
}


- (NSData *)dataDrawingPath: (CGPoint)point set:(BOOL)set start:(BOOL)start end:(BOOL)end ack:(BOOL)ack{
    unsigned int tmp = 12;
    char msg[12];

    msg[0] = 0x10;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);

    memcpy(msg + 4, &tmp, 4);
    
    tmp = [self makePacketDrawingContent:point];
    memcpy(msg + 8, &tmp, 4);
    
    msg[8] = [self makePacketDrawingHeader:set start:start end:end ack:ack];

    return [NSData dataWithBytes:msg length:12];
}

- (NSData *)dataDrawingPath: (CGPoint)point start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    return [self dataDrawingPath:point set:NO start:start end:end ack:ack];
}

- (NSData *)dataDrawingPath: (CGPoint)point color: (UIColor *)color size: (char)size start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    char msg[16];
    
    const char *tmp = [[self dataDrawingPath: point set:YES start:start end:end ack:ack] bytes];
    
    memcpy(msg, tmp, 12);
    
    unsigned int dataLength = 16;
    
    memcpy(msg + 4, &dataLength, 4);
    
    int detail = [self makePacketDrawingSetting:color size:size];
    memcpy(msg + 12, &detail, 4);
    
    return [NSData dataWithBytes:msg length:16];
}

- (int)makePacketDrawingContent: (CGPoint)point {
    return htonl((((((unsigned int)point.x) & 0xFFF) << 12) | ((((unsigned int)point.y) & 0xFFF))) & 0xFFFFFF);
}

- (int)makePacketDrawingSetting:(UIColor *)color size: (char)size {
    char msg[4];
    
    const float *colors = CGColorGetComponents(color.CGColor);
    
    msg[0] = (char) (colors[0] * 255.) & 0xFF;
    msg[1] = (char) (colors[1] * 255.) & 0xFF;
    msg[2] = (char) (colors[2] * 255.) & 0xFF;
    msg[3] = (((((char)(colors[3] * 127.)) & 0xF) << 4) | ((char) size & 0xF));
    
    return *(int*)msg;
}

- (char)makePacketDrawingHeader: (BOOL)set start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    char msg = 0;
    
    if (set) {
        bitc_set(msg, 0);
    }
    
    if (start) {
        bitc_set(msg, 1);
    }
    
    if (end) {
        bitc_set(msg, 2);
    }
    
    if (ack) {
        bitc_set(msg, 4);
    } else {
        bitc_set(msg, 3);
    }
    
    return msg;
}

- (CGPoint)drawingGetPoint: (const char *)fourByte {
    int tmp = ntohl(*(int *)(fourByte));
    CGPoint point;
    tmp &= 0xFFFFFF;
    point.x = tmp >> 12 & 0xFFF;
    point.y = tmp & 0xFFF;
    return point;
}

- (UIColor *)drawingGetColor: (const char *)nextFourByte {
    return [UIColor colorWithRed:(nextFourByte[0] / 255.) green:(nextFourByte[1] / 255.) blue:(nextFourByte[2] / 255.) alpha:((nextFourByte[3] >> 4) / 127.)];
}

- (char)drawingGetSize: (const char *)nextFourByte {
    return nextFourByte[3] & 0xF;
}

- (BOOL)drawingIsSet: (const char)header {
    return (bitc_get(header, 0) ? YES : NO);
}

- (BOOL)drawingIsStart: (const char)header {
    return (bitc_get(header, 1) ? YES : NO);
}

- (BOOL)drawingIsEnd: (const char)header {
    return (bitc_get(header, 2) ? YES : NO);
}

- (BOOL)drawingIsSyn: (const char)header {
    return (bitc_get(header, 3) ? YES : NO);
}

- (BOOL)drawingIsAck: (const char)header {
    return (bitc_get(header, 4) ? YES : NO);
}

- (void)handlePathDrawing: (NSData *)data {
    const char *msg = [data bytes];
    CGPoint point;
    UIColor *color;
    char size;
    BOOL ret;
    
    point = [self drawingGetPoint: msg + 8];
    
    if ([self drawingIsSet:msg[8]]) {
        //NSLog(@"hi");
        color = [self drawingGetColor:msg + 12];
        size = [self drawingGetSize:msg + 12];
        
        ret = [self.delegate drawPathSetting:color size:size];
        if (ret == NO)
            return;
    }

    ret = [self.delegate drawPath:point status:([self drawingIsStart:msg[8]] ? 1 : ([self drawingIsEnd:msg[8]] ? 2 : 0))];

    if (ret == NO)
        return;

    //Handling echo mechanism
    if ([self drawingIsSyn:msg[8]]) {
        if ([self drawingIsSet:msg[8]]) {
            [self.connection sendData:[self dataDrawingPath:point color:color size:size start:([self drawingIsStart:msg[8]]) end:([self drawingIsEnd:msg[8]]) ack:YES]];
        } else {
            [self.connection sendData:[self dataDrawingPath:point start:([self drawingIsStart:msg[8]]) end:([self drawingIsEnd:msg[8]]) ack:YES]];
        }
    }
    
    return;
}

- (void)closeConnect
{
    [self.connection close];
}

@end