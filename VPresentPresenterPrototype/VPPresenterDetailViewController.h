//
//  DetailViewController.h
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 18/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPPresenterSplitViewController.h"
//#import "VPSlidesViewController.h"
#import "Timer.h"
#import "VPCanvas.h"
#import <UIKit/UIKit.h>
#import <PSPDFKit/PSPDFKit.h>

@interface VPPresenterDetailViewController : UIViewController <UISplitViewControllerDelegate, PSPDFViewControllerDelegate, PSPDFAnnotationToolbarDelegate>

//@property (strong, nonatomic) id detailItem;
//@property (strong, nonatomic) IBOutlet UISwitch *monitorOutputSwitch;
//@property (strong, nonatomic) IBOutlet UILabel *connectExternalMonitor;
//@property (strong, nonatomic) IBOutlet UITextField *indexFileTextField;
//@property (strong, nonatomic) IBOutlet UINavigationItem *topNavigationItem;
//@property (nonatomic, strong) Timer* presentTimer;

// ViewDemo UI Elements
@property (strong, nonatomic) IBOutlet UIView *mainSlidesContainer;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UILabel *presenterLabel;

@property (strong, nonatomic) UIView *slides;

// ViewDemo Data
@property (nonatomic) NSInteger numberOfScreen;

@property (nonatomic) NSInteger currentImageIndex;
@property (nonatomic, strong) NSMutableArray* imageList;
@property (nonatomic, strong) NSMutableString* debugString;

// End of ViewDemo Elements and Data

// Function Prototype for other class to call
- (void)appendLog: (NSString *) msg;
- (void)reloadImage;
- (void)loadImage: (int)imageIndex;
- (void)loadPDF: (PSPDFDocument *)document;

@end
