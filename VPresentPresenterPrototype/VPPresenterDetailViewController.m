//
//  DetailViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 18/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPPresenterDetailViewController.h"
#import "VPPresenterSlidesListViewController.h"

#define DRAW_POINT_FREQ 4

@interface VPPresenterDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@property (strong, nonatomic) VPPresenterSplitViewController* splitVC;

@property (nonatomic) int touchCount;
@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (strong, nonatomic) PSPDFViewController *pdfViewController; 
@end


@implementation VPPresenterDetailViewController

// ViewDemo synthesize
@synthesize mainSlidesContainer;
@synthesize slides;
@synthesize imageList;
@synthesize timerLabel;
@synthesize debugString;
@synthesize splitVC;
@synthesize pdfDocument;
@synthesize pdfViewController;
// End of ViewDemo synthesize

@synthesize touchCount; 

// Original ViewDemo Contents
- (void) appendLog: (NSString *) msg{
    if (self.debugString == Nil) {
        self.debugString = [[NSMutableString alloc]initWithString:msg];
    } else {
        [self.debugString appendString:msg];
    }
}

//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    CGPoint point = [[touches anyObject] locationInView:self.mainSlidesContainer];
//
//    self.touchCount = 0;
//
//    VPCanvas *canvas = (VPCanvas *)([self.slides viewWithTag:124]);
//    
//    // Immediate drawing
//    [canvas penDown:point];
//
//    if (canvas.update) {
//        [self.splitVC.networkAgent sendDrawPath:point color:canvas.color size:canvas.thickness start:YES end:NO];
//    } else {
//        [self.splitVC.networkAgent sendDrawPath:point start:YES end:NO];
//    }
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    CGPoint point = [[touches anyObject] locationInView:self.mainSlidesContainer];
//    
//    self.touchCount += 1;
//    
//    if (self.touchCount % DRAW_POINT_FREQ)
//        return;
//        
//    [self.splitVC.networkAgent sendDrawPath:point start:NO end:NO];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.splitVC = (VPPresenterSplitViewController*)self.splitViewController;
    
    CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};

    self.slides = [[UIView alloc] initWithFrame: frame];
    slides.backgroundColor = [UIColor greenColor];
    
    [mainSlidesContainer addSubview: slides];
    
    self.pdfViewController = [[PSPDFViewController alloc] init];
    self.pdfViewController.rotationLockEnabled = YES;
    self.pdfViewController.textSelectionEnabled = NO;
    self.pdfViewController.linkAction = nil;
    self.pdfViewController.imageSelectionEnabled = NO;
    self.pdfViewController.delegate = self; 
    [self.pdfViewController.view setFrame:frame];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.pdfViewController];
    [navController.view setFrame:frame];
    
    [self addChildViewController:navController];
    [self.slides addSubview:navController.view];
    
    self.pdfViewController.leftBarButtonItems = nil;

    NSMutableArray *arr = [self.pdfViewController.rightBarButtonItems mutableCopy];
    [arr addObject:self.pdfViewController.annotationButtonItem];
    self.pdfViewController.annotationButtonItem.annotationToolbar.delegate = self;
    self.pdfViewController.rightBarButtonItems = arr;

    [self configureView];
    return;
}

//- (void)pdfViewController:(PSPDFViewController *)pdfController willDisplayDocument:(PSPDFDocument *)document {
//    NSLog(@"willDisplayDocument:");
//    
//}

- (void)pdfViewController:(PSPDFViewController *)pdfController didShowPageView:(PSPDFPageView *)pageView {
    [self.splitVC.networkAgent sendControlToSlide:self.pdfViewController.page];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didLoadPageView:(PSPDFPageView *)pageView {
    [self.splitVC.networkAgent sendControlToSlide:self.pdfViewController.page];
}

- (BOOL)pdfViewController:(PSPDFViewController *)pdfController shouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    //Disable selection
    return NO;
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didEndPageZooming:(UIScrollView *)scrollView atScale:(CGFloat)scale {
    PSPDFViewState *viewState = pdfController.viewState;
    [self.splitVC.networkAgent sendPDFViewState:viewState];
//    viewState.contentOffset = CGPointMakeScale(viewState.contentOffset, 1, self.externalViewZoomRatio); // viewState.contentOffset;
//    [self.externalPdfViewController setViewState:viewState animated:YES];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    PSPDFViewState *viewState = pdfController.viewState;
    [self.splitVC.networkAgent sendPDFViewState:viewState];
//    viewState.contentOffset = CGPointMakeScale(viewState.contentOffset, 1, self.externalViewZoomRatio);
//    NSLog(@"hi");
//    [self.externalPdfViewController setViewState:viewState animated:YES];
}


- (void)annotationToolbar:(PSPDFAnnotationToolbar *)annotationToolbar didChangeMode:(PSPDFAnnotationToolbarMode)newMode {
    if (newMode == PSPDFAnnotationToolbarNone) {
        [self synchronizeDirtyAnnotation];
    }
}

- (void)synchronizeDirtyAnnotation {
    NSDictionary *dirtyAnnotations = [self.pdfDocument.annotationParser dirtyAnnotations];
    for (NSNumber *key in dirtyAnnotations) {
        [self.splitVC.networkAgent sendPDFAnnotation:[[dirtyAnnotations objectForKey:key] lastObject] onPage:[key intValue]];
    }
    
    [self.pdfDocument saveChangedAnnotationsWithError:nil];
}

- (void)loadPDF: (PSPDFDocument *)document {
    NSLog(@"Ready to load PDF: %@", [document description]);
    self.pdfViewController.document = document;
    self.pdfDocument = document;
    [self.pdfViewController reloadData];
}

- (void)reloadImage {
    NSLog(@"!! reloadImage is NOT used"); 
//    VPCanvas *oldCanvas = (VPCanvas *) [self.slides viewWithTag:124];
//    
//    [oldCanvas removeFromSuperview];
//    [[self.slides viewWithTag:124] removeFromSuperview];
//    
//    // Create subview for new image
//    CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
//    
//    UIImage *tempImage = [UIImage imageWithContentsOfFile:[self.splitVC.imageList objectAtIndex:self.splitVC.currentImageIndex]];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage: tempImage];
//    imageView.frame = frame;
//    imageView.clipsToBounds = YES;
//    imageView.tag = 123;
//    [self.slides addSubview:imageView];
//    
//    // Send slide to moderator
//    [self.splitVC.networkAgent sendSlide: tempImage];
//    
//    VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
//    canvas.color = oldCanvas.color;
//    canvas.thickness = oldCanvas.thickness;
//    
//    canvas.tag = 124;
//    [self.slides addSubview:canvas];
}

- (void)loadImage: (int)imageIndex {
//    self.splitVC.currentImageIndex = imageIndex;
//    [self reloadImage];
    
    return; 
}

//- (IBAction)btnPrevSlide:(UIButton *)sender {
//    if (splitVC.imageList.count == 0) {
//        [splitVC showErrorPrompt:@"No Slide" description:@"Please load slide first. "];
//        [self appendLog:@"Not yet load slide!"];
//        return;
//    }
//    
//    if (splitVC.currentImageIndex <= 0) {
//        [splitVC showErrorPrompt:@"Cannot go to back" description:@"This is the first slide. "];
//        [self appendLog:@"No previous slide available"];
//        return;
//    }
//    
//    splitVC.currentImageIndex--;
//    
//    // Update Slide List View
//    UINavigationController *naviVC = [self.splitVC.viewControllers objectAtIndex:0];
//    UITabBarController * tabBarVC = [naviVC.viewControllers objectAtIndex:0];
//    VPPresenterSlidesListViewController *listVC = [tabBarVC.viewControllers objectAtIndex:1];
//    [listVC reloadFileListView];
//    
//    // Update Image
//    [self reloadImage];
//
//    return;
//}
//
//- (IBAction)btnNextSlide:(UIButton *)sender {
//    if (splitVC.imageList.count == 0) {
//        [splitVC showErrorPrompt:@"No Slide" description:@"Please load slide first. "];
//        [self appendLog:@"Not yet load slide!"];
//        return;
//    }
//    
//    if (splitVC.imageList.count <= splitVC.currentImageIndex + 1) {
//        [splitVC showErrorPrompt:@"Cannot go to next" description:@"This is the last slide. "];
//        [self appendLog:@"No next slide available"];
//        return;
//    }
//        
//    splitVC.currentImageIndex++;
//    
//    // Update Slide List View
//    UINavigationController *naviVC = [self.splitVC.viewControllers objectAtIndex:0];
//    UITabBarController * tabBarVC = [naviVC.viewControllers objectAtIndex:0];
//    VPPresenterSlidesListViewController *listVC = [tabBarVC.viewControllers objectAtIndex:1];
//    [listVC reloadFileListView];
//    
//    // Update Image
//    [self reloadImage];
//    
//    return;
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

#pragma mark - Managing the detail item
- (void)configureView
{
    // Update the user interface for the detail item.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Menu", @"Menu");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
    //return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setMainSlidesContainer:nil];
    [self setPresenterLabel:nil];
    [super viewDidUnload];
}
@end
